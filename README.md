...work in progress

###TODO: ###
* Investigate issue with the Symlink's and max path lenght in OSX...
* add to this **readme** vagrant commands like - `vagrant destroy -f`,
* ...

-----------------------------------

# README #

**Docker Project tree**
```
** Nextgen Projects **
├── nextgen_api
│   ├── app
│   ├── bin
│   ├── src
│   └── web
├── nextgen_frontend
│   ├── app
│   ├── bower.json
│   ├── config
│   ├── dist
│   ├── ember-cli-build.js
│   ├── node_modules
│   ├── package.json
│   ├── public
│   ├── README.md
│   ├── testem.js
│   └── vendor
│** Vagrant & Docker files **
├── bootstrap.sh
├── docker-compose.yml
├── node
│   └── Dockerfile
├── node_modules
│   └── Dockerfile
├── php
│   └── Dockerfile
├── README.md
└── Vagrantfile
```

## For WINDOWS USERS - VAGRANT ##
** Install (update) to lastest version of VirtualBox and Vagrant. **

* Run cmd(Cygwin/GitBash) as Administrator, and run command from the directory where **Vagrantfile** is located

`$ vagrant up && vagrant ssh`

In virtual host machine go to shared folder from Windows host with project:
`cd /vagrant`
-----------------------------------

## Alias in host machine ##

(set in "bootstrap.sh")

### Backend alias ###

* **dc** = 'docker-compose'
* **php** = 'docker-compose run php php'
* **composer** = 'docker-compose run php composer'
* **dcstop** = 'docker-compose stop && docker-compose rm -f && docker-compose build&& docker-compose up -d'

### Frontend alias ###

* **npm** = 'docker-compose run npm'
* **bower** = 'docker-compose run bower'
* **ember** = 'docker-compose run ember'
* **node** = 'docker-compose run node'

----------------------------------------

## For LINUX USERS - DOCKER##

Install **docker** and run `docker-compose run npm install` (or alias `npm install`) in directory whith docker-composer.yml file...


```
#!bash

$ curl -fsSL https://get.docker.com/ | sh
$ usermod -aG docker vagrant
$ curl -L https://github.com/docker/compose/releases/download/1.6.2/docker-compose-`uname -s`-`uname -$ m` > /usr/local/bin/docker-compose
$ chmod +x /usr/local/bin/docker-compose
```


---------------------------------------

### Docker Up and workflow: ###
* All commands from directory whith docker-compose.yml file.

`$ docker-compose build`(use alias `dc build`) 

**Frontend**


`$ docker-compose run npm install` (use alias `npm install`) 

`$ docker-compose run bower install` (use alias `bower install`) 


`$ ...` from readme in gitlab

Then Up Ember server ('-d' detach mode) (if ember linked to php то Up and running php-apache2, mysql server) :

`$ docker-compose up -d` (use alias `dc up -d`)

**View logs**

`$ docker-compose logs` (use alias `dc logs`)


**Backend**


`$ docker-compose up -d` (`dc up -d`)

`$ docker-compose logs` (`dc logs`)

`$ ...` from readme in gitlab (`$ php composer install`)

### Example: ###
* `$ npm install`
* `$ bower install`
* `$ ember -v`
* `$ php composer install`
* `$ php app/console debug:router`
* `$ docker-compose run bower install`

## Some links ##
* [some-issues-with-vagrant](https://aigeec.com/some-issues-with-vagrant/)
* [How To Add Swap on Ubuntu 14.04](https://www.digitalocean.com/community/tutorials/how-to-add-swap-on-ubuntu-14-04)