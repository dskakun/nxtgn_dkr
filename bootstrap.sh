#!/bin/bash

# Install docker and docker compose
echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
apt-get update&&apt-get install -y -qq git
curl -fsSL https://get.docker.com/ | sh
usermod -aG docker vagrant
curl -L https://github.com/docker/compose/releases/download/1.6.2/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

#Add SWAP file 4Gb
sudo fallocate -l 4G /swapfile 
sudo chmod 600 /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile
sudo bash -c "echo '/swapfile   none    swap    sw    0   0' >> /etc/fstab "

#----      Add alias's      ----#
npm="alias npm='docker-compose run npm'";
bower="alias bower='docker-compose run bower'";
ember="alias ember='docker-compose run ember'";
node="alias node='docker-compose run node'";
dc="alias dc='docker-compose'";
php="alias php='docker-compose run php php'";
composer="alias composer='docker-compose run php composer'";
dcstop="alias dcstop='docker-compose stop && docker-compose rm -f && docker-compose build&& docker-compose up -d'";

cat >> /home/vagrant/.bash_aliases << EOL
${npm}
${bower}
${ember}
${node}
${dc}
${php}
${composer}
${dcstop}
EOL

mkdir -p /home/vagrant/tmp_node